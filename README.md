# test_labyrinthe

Déscription de l'algorithme :

- Persée est situé à un point P du labyrinthe
- Persée essaye toujours d'avancer sur un nouveau point Pn.
- Si Persée est bloqué pour son avancement, il doit retourné vers le Point Pn-1; c'est le point où il a été avant d'arriver au point Persée.
- Si Persée retourne au point Pn-1, il ne doit absolument pas avancer vers ce point P.
- Pour essayer d'emprunter le chemin le plus court, Persée doit tout d'abord vérifier s'il peut avancer d'une colonne vers l'avant ou monter d'une ligne.