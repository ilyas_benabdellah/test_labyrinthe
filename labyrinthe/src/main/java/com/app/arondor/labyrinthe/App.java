package com.app.arondor.labyrinthe;

import java.util.ArrayList;
import java.util.List;

/**
 * Algo: Sortir du labyrinth
 *
 */
public class App {

	// listePointsRetournes: est la liste des points où Percée a été obligé d'y
	// laisser. Exemple :
	// Persée se trouve dans une situation où il est bloqué sur un Point P, il est
	// donc obligé de retourner vers le point P-1
	private static List<Point> listePointsRetournes = new ArrayList<Point>();

	// listePointsAGarder: est la liste des points où Percée doit passer pour sortir
	// du labyrinthe
	private static List<Point> listePointsAGarder = new ArrayList<Point>();

	// pointAGarder : est le point qu'on garde pour trouver le chemin de sortie
	// On instancie ce point par la position actuelle de Persée avant l'avancement
	// vers la nouvelle postion
	private static Point pointAGarder = new Point();

	private static int N = 6;

	public static void main(String[] args) {

		// Cet algorithme traite un cas passant c'est à dire qu'il existe au moins un
		// chemin pour passer de l'entrée à la sortie

		// Scanner in = new Scanner(System.in);
		// System.out.println("Entrer la taille du tableau");
		// N = in.nextInt();

		boolean[][] labyrinthe = new boolean[N][N];

//		for (int i = 0; i < N; i++) {
//			for (int j = 0; j < N; j++) {
//
//				System.out.println("Entrer la valeur de l'élement : ( " + i + " , " + j + " ):");
//				labyrinthe[i][j] = in.nextBoolean();
//			}
//		}

		labyrinthe[0][0] = false;
		labyrinthe[0][1] = false;
		labyrinthe[0][2] = false;
		labyrinthe[0][3] = true;
		labyrinthe[0][4] = false;
		labyrinthe[0][5] = false;
		labyrinthe[1][0] = false;
		labyrinthe[1][1] = false;
		labyrinthe[1][2] = false;
		labyrinthe[1][3] = false;
		labyrinthe[1][4] = false;
		labyrinthe[1][5] = true;
		labyrinthe[2][0] = false;
		labyrinthe[2][1] = true;
		labyrinthe[2][2] = false;
		labyrinthe[2][3] = true;
		labyrinthe[2][4] = true;
		labyrinthe[2][5] = false;
		labyrinthe[3][0] = false;
		labyrinthe[3][1] = false;
		labyrinthe[3][2] = true;
		labyrinthe[3][3] = false;
		labyrinthe[3][4] = false;
		labyrinthe[3][5] = false;
		labyrinthe[4][0] = true;
		labyrinthe[4][1] = false;
		labyrinthe[4][2] = false;
		labyrinthe[4][3] = true;
		labyrinthe[4][4] = true;
		labyrinthe[4][5] = false;
		labyrinthe[5][0] = false;
		labyrinthe[5][1] = false;
		labyrinthe[5][2] = false;
		labyrinthe[5][3] = false;
		labyrinthe[5][4] = false;
		labyrinthe[5][5] = true;

		// Méthode pour sortir du labyrinthe
		echappeToi(labyrinthe);

		for (Point pc : listePointsAGarder) {
			System.out.println("Persée doit passé par ce point : (" + pc.getC() + "," + pc.getL() + ")");
		}

		System.out.println("Persée est sorti");

	}

	private static List<Point> echappeToi(boolean[][] labyrinthe) {

		// Position initiale
		Point p = new Point(0, N - 1);

		// Variable qui définit si Persée peut sortir du labyrithe ou pas
		boolean sortirLabyrinthe = false;

		while (sortirLabyrinthe == false) {

			// Cette variable définit si on a un avancement vers un nouveau point (jamais
			// passé auparavant) ou non
			boolean avancement = false;

			// Ici on définit les différentes points où peut aller Persée

			Point p1 = new Point(p.getC() + 1, p.getL());
			Point p2 = new Point(p.getC(), p.getL() - 1);
			Point p3 = new Point(p.getC(), p.getL() + 1);
			Point p4 = new Point(p.getC() - 1, p.getL());

			if (p.getC() == N - 1 && p.getL() == 0) {
				sortirLabyrinthe = true;
				continue;
			}

			// Ici il y'a les différents choix que Persée peut choisir pour avancer ou
			// retourner.

			// Généralement Persée a 4 choix sauf lorsqu'il est dans la première ligne /
			// colonne
			// ou la dernière ligne / colonne (dans ces cas là il a 2 ou 3 choix)

			// Bien sûre Persée ne doit pas revenir de là où il venait d'où par exemple
			// cette condition :
			// !(pointAGarder.getC() == p.getC() + 1 && pointAGarder.getL() == p.getL())

			// A noter aussi que persée ne doit pas avancer vers un point où il a été obligé
			// d'y retourner (Sinon il y'aura une boucle infinie), d'où la condition:
			// verifierExistencePoint(p1)

			// Le choix de ces deux If en premier lieu a pour objectif d'éssayer d'emprunter
			// le chemin
			// le plus court ; Soit avancer d'une colonne (1ère condition), soit monter
			// d'une ligne (2ème condition) :

			if (p.getC() != N - 1 && labyrinthe[p.getC() + 1][p.getL()] == false && !(verifierExistencePoint(p1))
					&& !(pointAGarder.getC() == p.getC() + 1 && pointAGarder.getL() == p.getL())) {
				avancement = true;
				garderLePoint(p);
				listePointsAGarder.add(pointAGarder);
				p = p1;
				System.out.println("Persée est ici : (" + p.getC() + "," + p.getL() + ")");
				continue;
			}

			if (p.getL() != 0 && labyrinthe[p.getC()][p.getL() - 1] == false && !(verifierExistencePoint(p2))
					&& !(pointAGarder.getC() == p.getC() && pointAGarder.getL() == p.getL() - 1)) {
				avancement = true;
				garderLePoint(p);
				listePointsAGarder.add(pointAGarder);
				p = p2;
				System.out.println("Persée est ici : (" + p.getC() + "," + p.getL() + ")");
				continue;
			}

			if (p.getL() != N - 1 && labyrinthe[p.getC()][p.getL() + 1] == false && !(verifierExistencePoint(p3))
					&& !(pointAGarder.getC() == p.getC() && pointAGarder.getL() == p.getL() + 1)) {
				avancement = true;
				garderLePoint(p);
				listePointsAGarder.add(pointAGarder);
				p = p3;
				System.out.println("Persée est ici : (" + p.getC() + "," + p.getL() + ")");
				continue;
			}

			if (p.getC() != 0 && labyrinthe[p.getC() - 1][p.getL()] == false && !(verifierExistencePoint(p4))
					&& !(pointAGarder.getC() == p.getC() - 1 && pointAGarder.getL() == p.getL())) {
				avancement = true;
				garderLePoint(p);
				listePointsAGarder.add(pointAGarder);
				p = p4;
				System.out.println("Persée est ici : (" + p.getC() + "," + p.getL() + ")");
				continue;
			}

			// Cette condition est valide que si Persée est obligé d'y retourner vers sa
			// dernière position
			if (avancement == false) {
				System.out.println("Persée doit retourner vers l'avant dernière position");
				ajouterPointsRetournes(p);
				p = pointAGarder;
				// L'appel à la méthode changerPointAGarder() c'est pour s'assurer de ne garder
				// que les points où doit passer Persée
				changerPointAGarder(pointAGarder);
				System.out.println("Persée est ici : (" + p.getC() + "," + p.getL() + ")");
			}

		}

		return listePointsAGarder;

	}

	private static void garderLePoint(Point p) {
		pointAGarder = p;
	}

	private static void changerPointAGarder(Point p) {

		if (listePointsAGarder != null && listePointsAGarder.size() > 1) {
			pointAGarder = listePointsAGarder.get(listePointsAGarder.size() - 2);
			listePointsAGarder.remove(listePointsAGarder.size() - 1);
		}
	}

	// Cette méthode sert à ajouter les points lesquels Persée a été obligé d'y
	// retourner et pas avancer vers un nouveau point
	private static List<Point> ajouterPointsRetournes(Point p) {
		if (listePointsRetournes != null) {
			listePointsRetournes.add(p);
			return listePointsRetournes;
		} else {
			return null;
		}

	}

	// Cette méthode est créee pour ne pas retourner vers le même chemin passé
	private static boolean verifierExistencePoint(Point p) {

		for (Point pl : listePointsRetournes) {
			if (p.getC() == pl.getC() && p.getL() == pl.getL()) {
				return true;
			}
		}

		return false;
	}

}
