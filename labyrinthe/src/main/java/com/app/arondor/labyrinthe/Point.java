package com.app.arondor.labyrinthe;

public class Point {
	
	// Variable pour colonne
	private int c;
	// Variable pour ligne
	private int l;

	public Point(int c, int l) {
		super();
		this.c = c;
		this.l = l;
	}

	public Point() {
		super();
	}

	public int getC() {
		return c;
	}

	public void setC(int c) {
		this.c = c;
	}

	public int getL() {
		return l;
	}

	public void setL(int l) {
		this.l = l;
	}



}
